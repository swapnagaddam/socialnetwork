package mySocialNetworking;
import java.time.LocalDateTime;
import java.util.ArrayList;
/**
 * To create a Message class to implement the messages
 * by using ArrayList
 * @author swgaddam1
 *
 */
public class Message {
	ArrayList<String> messageList = new ArrayList<String>();
	private String sendmessage1;
	private String recievemsg1;
	private String msgbody1;
	LocalDateTime timeStamp;
/**
 * To create a constructor with parameters 
 * using this statement	
 * @param sender
 * @param reciever
 * @param msgbody
 * @param timeStamp
 */
public Message(String sender, String reciever, String msgbody, LocalDateTime timeStamp){
	this.sendmessage1=sender;    //using this statement
	this.recievemsg1=reciever;   //using this statement
	this.msgbody1=msgbody;       //using this statement
	this.timeStamp=timeStamp;    //using this statement
	System.out.println(this);
	}
/**
 * override the default toString method of Message 
 * so that message can be printed in human readable format
 */
@Override
public String toString() {
	String returnToMe="";
	returnToMe+="sender      :      "+this.sendmessage1+  "\n";
	returnToMe+="reciever    :      "+this.recievemsg1+   "\n";
	returnToMe+="msgbody     :      "+this.msgbody1+      "\n";
	returnToMe+="timeStamp   :      "+this.timeStamp+     "\n";
	return returnToMe;
}
	
}

