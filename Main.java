package mySocialNetworking;

import java.time.LocalDateTime;

/**
 *Testing for WhatsApp application
 *by using all the classes
 * @author swgaddam1
 *
 */
public class Main {
	public static void main(String[] args) {
		UserCollection Uc=new UserCollection();
		Uc.createNewUser("swapna", "8179288008", "Keerthi@06");
		Uc.createNewUser("Rachana", "8688027633", "Rachu@10");
		Uc.createNewUser("sai", "9972456783", "Saaiiii@3");
		
		System.out.println("-----------sendMessagaes-------------");
	    System.out.println("");
		
	    Uc.sendMessage("8179288008","8688027633","hiii", LocalDateTime.now());
	    Uc.sendMessage("8688027633","9972456783","hello", LocalDateTime.now());
	    Uc.sendMessage("9972456783","8688027633","coffieee", LocalDateTime.now());
	    Uc.sendMessage("8179288008", "9972456783", "dinner?", LocalDateTime.now());
	}
}
