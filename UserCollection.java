package mySocialNetworking;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * To create a class for userCollection
 * @author swgaddam1
 *
 */
public class UserCollection {
	private ArrayList<User> userList;
	private ArrayList<Message> sendMessagesList;
	
	public UserCollection() {
		this.userList=new ArrayList<User>();
	}
	/**
	 * To create createNewUser method using parameters
	 * By using ArrayList and addUser
	 * @param name
	 * @param phoneNo
	 * @param password
	 * @return
	 */
	public boolean createNewUser(String name, String phoneNo, String password)
	{
		try
		{
			User addUser=new User(name, phoneNo, password);
			userList.add(addUser);
			return true;
		}catch(Exception e) {
		
		return false;	
	}
}
	/**
	 * To create a method  for printAllusers 
	 * By using Iterator to print the currentUser
	 */
	public void printAllusers() {
		for (Iterator<User> iter = userList.iterator();
				iter.hasNext();) {
			User currentUser = (User)iter.next();
			System.out.println(currentUser);
			}
	}
	/**
	 * To create a method for sendMessage
	 * By using sendMessagesList 
	 * @param sender
	 * @param reciever
	 * @param msgbody
	 * @param timeStamp
	 * @return
	 */
	public boolean sendMessage(String sender, String reciever, String msgbody, LocalDateTime timeStamp)
	{
			Message addSendMessages = new Message(sender, reciever, msgbody, timeStamp);
			if(sender.contains(reciever))
			{
		    sendMessagesList.add(addSendMessages);
			}
		
		   return false;	
	}
}
